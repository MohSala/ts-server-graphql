import "reflect-metadata";
import { MikroORM } from "@mikro-orm/core";
import { __prod__ } from "./constants";
import mikroOrmConfig from "./mikro-orm.config";
import express from "express";
import { ApolloServer } from "apollo-server-express";
import { buildSchema } from "type-graphql";
import { HelloResolver } from "./resolvers/hello";
import { PostResolver } from "./resolvers/post";
import { UserResolver } from "./resolvers/user";
import redis from 'redis';
import session from 'express-session';
import connectRedis from 'connect-redis';
const { ApolloServerPluginLandingPageGraphQLPlayground } = require('apollo-server-core');
import cors from "cors";
declare module 'express-session' {
    interface Session {
        userId: string;
    }
}

const main = async () => {
    const orm = await MikroORM.init(mikroOrmConfig);
    orm.getMigrator().up();

    const app = express();

    let RedisStore = connectRedis(session);
    let redisClient = redis.createClient()



    app.use(
        session({
            name: 'qid',
            store: new RedisStore({ client: redisClient, disableTouch: true }),
            cookie: {
                maxAge: 1000 * 60 * 60,
                httpOnly: true,
                sameSite: 'lax',
                secure: __prod__ // cookie only works with https
            },
            secret: "omologo5g",
            resave: false,
            saveUninitialized: false
        })
    )
    app.use(cors({
        origin: 'http://localhost:3000',
        credentials: true
    }));
    const apolloServer = new ApolloServer({
        plugins: [
            ApolloServerPluginLandingPageGraphQLPlayground({
                // options
            })],
        schema: await buildSchema({
            resolvers: [HelloResolver, PostResolver, UserResolver],
            validate: false,
        }),
        context: ({ req, res }) => ({ em: orm.em, req, res })
    })

    await apolloServer.start();

    apolloServer.applyMiddleware({ app, cors: false });

    app.listen(4000, () => {
        console.log("server started on 4000");
    })

}
main().catch(err => console.error(err));