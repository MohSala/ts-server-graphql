import { Query, Resolver } from "type-graphql";

//using type-graphql as our schema

@Resolver()
export class HelloResolver {
    @Query(() => String)
    hello() {
        return "bye world"
    }
}