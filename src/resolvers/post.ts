import { Post } from "../entities/Post";
import { MyContext } from "src/types";
import { Arg, Ctx, Field, InputType, Int, Mutation, ObjectType, Query, Resolver, } from "type-graphql";

//using type-graphql as our schema

@InputType()
class PostInput {
    @Field()
    title: string

    @Field()
    content: string
}

@ObjectType()
class FieldErr {
    @Field()
    field: string;

    @Field()
    message: string;

}

@ObjectType()
class PostResponse {
    @Field(() => [FieldErr], { nullable: true })
    errors?: FieldErr[]

    @Field(() => Post, { nullable: true })
    post?: Post
}

@Resolver()
export class PostResolver {
    @Query(() => [Post])
    posts(@Ctx() ctx: MyContext): Promise<Post[]> {
        return ctx.em.find(Post, {});
    }

    @Query(() => Post, { nullable: true })
    post(
        @Arg('id', () => Int) id: number,
        @Ctx() ctx: MyContext): Promise<Post | null> {
        return ctx.em.findOne(Post, { id });
    }

    @Mutation(() => PostResponse)
    async createPost(
        @Arg('options') option: PostInput,
        @Ctx() { em }: MyContext): Promise<PostResponse> {
        const post = em.create(Post, { title: option.title, content: option.content });
        await em.persistAndFlush(post);
        return { post };

    }

    @Mutation(() => Post, { nullable: true })
    async updatePost(
        @Arg('id', () => Int) id: number,
        @Arg('title', () => String, { nullable: true }) title: String,
        @Ctx() ctx: MyContext): Promise<Post | null> {
        const post = await ctx.em.findOne(Post, { id });
        if (!post) {
            return null;
        }
        if (typeof title !== 'undefined') {
            post.title = title;
            await ctx.em.persistAndFlush(post);

        }
        return post;
    }

    @Mutation(() => Boolean)
    async deletePost(
        @Arg('id', () => Int) id: number,
        @Ctx() ctx: MyContext): Promise<Boolean> {
        await ctx.em.nativeDelete(Post, { id });
        return true;
    }
}